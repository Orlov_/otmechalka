import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import moment from 'moment'

Object.defineProperty(Vue.prototype, '$moment', {value: moment});
require('moment/locale/ru');
moment.locale('ru');


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
