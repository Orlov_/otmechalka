import Vue from 'vue'
import VueRouter from 'vue-router'

import fillplease from '../views/fill_please.vue'
import checkpage from '../views/checkpage.vue'
import tablepage from '../views/tablepage.vue'
import studentpage from '../views/student_page.vue' 

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
      {
        path: '/',
        name: 'main',
        component: fillplease
      },
      {
        path: '/checkpage',
        name: 'check',
        component: checkpage
      },
      {
        path: '/tablepage',
        name: 'table',
        component: tablepage
      },
      {
        path: '/studentpage',
        name: 'student',
        component: studentpage
      }
  ]
})

export default router
